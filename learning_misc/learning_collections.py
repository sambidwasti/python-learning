""" deque/collection and various
"""
import numpy as np
import plotly.express as px
from collections import deque


#fig.add_trace(go.Scatter(x=list(self.fpga_rate_time), y=list(self.fpga_rates)))
N_ASIC = 2#12
N_CH = 3#32
N_ADC = 4#1024

CHANNEL_AXIS_VALS = np.array(range(192))
N_LAYERS = 2
ALL_LEGEND_LAYERS = []

RATE_UPDATE_RATE = 1  # Hz
FIGURE_LOOKPACK_TIME = 5
FIGURE_LOOKBACK_SAMPLES = FIGURE_LOOKPACK_TIME * RATE_UPDATE_RATE

def deque_1D():
    lt = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                maxlen=FIGURE_LOOKBACK_SAMPLES)

    lt_time = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                    maxlen=FIGURE_LOOKBACK_SAMPLES)

    rates = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                  maxlen=FIGURE_LOOKBACK_SAMPLES)
    #print(type(lt))
   #print(lt)
    #print(type(lt_time))
    #print(lt_time)
   # print(type(rates))
    #print(rates)

    lt.append(20)
    lt.append(25)
    #print(lt)

def deque_2D():
    xlist = [1,2,3,4,5]
    x = list(xlist)

    t_array = np.zeros(shape=(5,2))
    #print(t_array)
 #   print(type(t_array))
    lt2D = deque(t_array, maxlen=FIGURE_LOOKBACK_SAMPLES)
 #   print(type(lt2D))
  #  print(lt2D)

    lt2D.append(np.array([20,25]))
#    print(lt2D)
    lt2D.append(np.array([10,35]))
  #  print(lt2D)
    lt2D.append(np.array([30,45]))
  #  print(lt2D)
    lt2D.append(np.array([40,55]))
   # print(lt2D)
    #Now separate it in 2 lists.

  #  print("*")
    temp_list = np.array(lt2D)
    list0 = list(temp_list[:,0])
    list1 = list(temp_list[:,1])
    temp_main_list =[]
    temp_main_list.append(list0)
    temp_main_list.append(list1)
    #alllist=list(np.array(temp_main_list))
    alllist = temp_main_list
    #alllist = list(np.array([list0,list1]))
    print(list0)
    print(list1)
    print(alllist)
    fig = px.scatter(x=x,y=alllist)
    fig.show()

def deque_snapshot():
    """
    This is to test collection using the adc-snapshot of the interface
    """
    adc_history = np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int)
    print(type(adc_history))
    print(adc_history.shape)
    temp_array = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
          maxlen=FIGURE_LOOKBACK_SAMPLES)
    print(type(temp_array))
    print(np.array(temp_array).shape, 'kk')

    for i in range(5):
        data = np.random.rand(N_ASIC,N_CH)
        temp_array.append(data)
        print('---')
        print(i)
        print(data)
        print(temp_array)

    #print(data)
    #print(type(data))
    #print(data.shape)
    print(temp_array)


    temp_array1 = np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH, N_LAYERS), dtype=int)
    print(type(temp_array1))
    print(temp_array1.shape)
    temp_deque = deque(temp_array1, maxlen=FIGURE_LOOKBACK_SAMPLES)
    print(type(temp_deque))

if __name__ == "__main__":
    deque_snapshot()
