""" Changing Global Variable through Classes,etc.

Testing change of global variable through class.
"""

GLOBAL_VAR = 100.00

class Inter():

    def __init__(self):
        self.value_a = 1.0
        self.value_b = 2.0
        print("Inter Initialized")

    def change_global(self, val=90):
        """ Change the Global Variable through this function.

        Parameters
        ----------
        val : variable

        """
        global GLOBAL_VAR
        GLOBAL_VAR = val

def output_glo_var():
    """ Print the Global Variables.

    """
    print(GLOBAL_VAR)