"""This is to understand the command line arguments.

    >>> Usage: python cmd_args1.py HE LL O
"""
import sys

if __name__ == "__main__":
    print(type(sys.argv))
    print(' The command line arguments are : ')
    for i in sys.argv:
        print(i)
