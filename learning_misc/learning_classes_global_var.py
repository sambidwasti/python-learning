""" Change Global variables in another script
"""
import numpy as np
import plotly.express as px
from collections import deque
import silayer.raw2hdf as raw2hdf
import learning_classes_main

def deque_try():
    RATE_UPDATE_RATE = 1  # Hz
    FIGURE_LOOKPACK_TIME = 15
    FIGURE_LOOKBACK_SAMPLES = FIGURE_LOOKPACK_TIME * RATE_UPDATE_RATE

    N_ASIC = 12
    N_CH = 32
    N_ADC = 1024
    CHANNEL_AXIS_VALS = np.array(range(192))

    lt = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                maxlen=FIGURE_LOOKBACK_SAMPLES)

    lt_time = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                    maxlen=FIGURE_LOOKBACK_SAMPLES)

    rates = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                  maxlen=FIGURE_LOOKBACK_SAMPLES)

    print(type(lt))
    print(lt)
    print(type(lt_time))
    print(lt_time)
    print(type(rates))
    print(rates)

    hist_bins = np.array(range(N_ADC), dtype=int)
    adc_histograms = np.zeros((N_ASIC, N_CH, N_ADC), dtype=int)
    histogram_stats = np.zeros((N_ASIC, N_CH, 2), dtype=float)
    print(type(hist_bins))
    print(hist_bins.shape)
    print(type(adc_histograms))
    print(adc_histograms.shape)
    print(type(histogram_stats))
    print(histogram_stats.shape)

    adc_history = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                        maxlen=FIGURE_LOOKBACK_SAMPLES)
    print('ADC-History=',type(adc_history))
    print(np.array(adc_history).shape)
    #data = raw2hdf.simple_parse_packet(0)
    #print(type(data))

    ltarray = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES,N_ASIC),dtype=float), maxlen=FIGURE_LOOKBACK_SAMPLES)
    print(type(ltarray))
    print(ltarray)
    #<class 'numpy.ndarray'>
    #(12,)


def changing_global_var():
    initial = learning_classes_main.Inter()
    delme2.output_glo_var()
    initial.change_global(val=200)
    delme2.output_glo_var()

deque_try()

