""" Few Array issues.

write the csv file.
close csv file.
append csv file.
rename csv file.
"""
import csv
import numpy as np
import plotly.express as px
import os

INDEX = np.array([1,2,3,4,5,6,7,8,9,10])
NAME  = ["Apple","Ball","Cat","Dog","Elephant","Fish","Gun","Horse","Indigo","Joker"]
SCORE = np.array([86,90,20,39,53,66,96,53,86,89])

B = []
def temp1():
    main_arr = [[4500,0,0,0,0,0],
                [0,4499,0,0,0,0],
                [4502  ,  0   , 0  ,  0   , 0 ,   0],
                [   0 ,4501  ,  0  ,  0,    0  ,  0]]

    main_arr = np.array(main_arr)
    print(main_arr)
    print(type(main_arr))
    for i in range(2):
        temp_val = main_arr[:,i]
        print(temp_val.shape)
        print(type(temp_val))
        print(temp_val)
        print(i)
        layer_event_counter = temp_val[temp_val != 0]  #
        print(layer_event_counter)

def temp2(val,layer):
    temp_list = [0,0,0,0,0,0]
    #print(temp_list)
    #print(type(temp_list))
    temp_list[layer]=val

    B.append(temp_list)
    #print(B)
    #print(type(B))



def temp3():
    print('**** TEMP 3 ****')
    c = np.array(B)
    print(c)
    print(type(c))
    print(c.shape)
    for i in range(6):
        print(i)
        layer_column = c[:,i]
        print(layer_column)
        print(type(layer_column))
        newval = layer_column[layer_column !=0]
        print(newval)
        print('---')

def temp4():
    temp()
    val = 20
    layer = 2
    temp2(val, layer)

    val = 10
    layer = 3
    temp2(val, layer)

    val = 40
    layer = 1
    temp2(val, layer)
    val = 11
    layer = 0
    temp2(val, layer)
    val = 50
    layer = 2
    temp2(val, layer)

    val = 30
    layer = 2
    temp2(val, layer)

    val = 40
    layer = 2
    temp2(val, layer)
    temp3()

def temp5():
    x1 = [1,2,3,4,5,6,7,8,9,10]
    x2 = [2,3,5,6,7,11,13,14,5,32]
    y1 = [12,34,5,12,44,23,56,77,88,55]
    y2 = [23,44,56,77,88,93, 44,32,88,55]
    x = x1
    y = [y1,y2]

    fig = px.scatter(x=x,y=y)
    fig.update_traces(mode='lines+markers')

    fig.show()

if __name__ == "__main__":
    temp5()