""" Learning CSV file
"""
import csv
import numpy as np
import os

INDEX = np.array([1,2,3,4,5,6,7,8,9,10])
NAME  = ["Apple","Ball","Cat","Dog","Elephant","Fish","Gun","Horse","Indigo","Joker"]
SCORE = np.array([86,90,20,39,53,66,96,53,86,89])


def read_csv_file(csv_filename):
    """ Read CSV file.

    This has not been tested extensively.

    Parameters
    ----------
    csv_filename : csv filename

    """
    with open(csv_filename,'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        #csv_reader=csv.DictReader(csv_file)
        next(csv_reader) # skips the first line (headers)

        for line in csv_reader:
            print(line)
            #print(line[2]) # prints the third field/column.

def write_csv_file_dict(csv_filename):
    """Create and write a csv file form dictionary.
    """
    with open(csv_filename,'w') as new_file:
        fieldnames = ['Index', 'Name', 'Score']
        csv_writer = csv.DictWriter(new_file, fieldnames=fieldnames)
        csv_writer.writeheader()

    #    csv_writer = csv.writer(new_file) #changing delimiter to ;, tab = \t

def append_csv_file_dict(csv_filename):
    """ Append the already created csv file.

    This is using the dictionary.

    Parameters
    ----------
    csv_filename : csv filename

    Returns
    -------

    """
    with open(csv_filename, 'a+') as old_file:
        fieldnames = ['Index', 'Name', 'Score']
        csv_writer = csv.DictWriter(old_file, fieldnames=fieldnames)
        for i in range(10):
            csv_writer.writerow({'Index':str(INDEX[i]), 'Name':NAME[i], 'Score':SCORE[i]})

def write_csv_file(csv_filename):
    """ Write a regular csv file."""
    with open(csv_filename,'w') as new_file:
        csv_writer = csv.writer(new_file)
        csv_writer.writerow(['Index','Name','Score'])

def append_csv_file(csv_filename):
    """ Append data to a already created csv file."""
    with open(csv_filename, 'a+') as old_file:
        csv_writer = csv.writer(old_file)
        for i in range(10):
            csv_writer.writerow([INDEX[i],NAME[i],SCORE[i]])

def change_csv_filename(csv_filename,new_filename='newfile'):
    """ Change the file name using a function."""
    cur_path = os.getcwd()
    cur_fname = cur_path+'/'+csv_filename
    print(cur_fname)
    new_fname = cur_path+'/'+new_filename + '.csv'
    print(new_fname)
    os.rename(cur_fname,new_fname)
    # get path and rename

if __name__ == "__main__":
    csv_filename = "test_csv.csv"

    write_csv_file_dict(csv_filename)
    append_csv_file_dict(csv_filename)

    #write_csv_file(csv_filename)
    #append_csv_file(csv_filename)

    change_csv_filename(csv_filename, new_filename='run1')

