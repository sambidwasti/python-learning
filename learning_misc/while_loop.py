"""While loop tutorial.

    Various test procedure to understand the loops.

"""

def wloop_test1():
    """ Testing basic while loop.
    """
    print("Test1: Print first 5 of the loop")
    i = 1
    while i < 6:
        print(i)
        i += 1


def wloop_test2():
    """ Testing while loop and Break.
    """
    print("Test 2: Testing Break( stops loop at 3)")
    i = 0
    while i < 6:
        i += 1
        if i == 3:
            break
    print(i)

def wloop_test3():
    """ Testing while loop and Continue.
    """
    print("Test3: Continue (skips 3)")
    i = 0
    while i < 6:
        i += 1
        if i == 3:
            continue
    print(i)

def wloop_test4():
    """ Testing while loop with else.
    """
    i = 0
    print(" The while also has an else statement")
    while i < 6:
        print(i)
        i += 1
    else:
        print("values are no longer greater than 6")

if __name__ == "__main__":
    print(" WHILE LOOP TUTORIAL")
    print(" Initiate Test1")
    wloop_test1()
    print(" Initiate Test2 : Break")
    wloop_test2()
    print(" Initiate Test3 : Continue")
    wloop_test3()
    print(" Initiate Test4 : Continue")
    wloop_test4()

