""" For learning/Understand various string manipulations."""


def string_split():
    """ Learning string split, using the plot string used in interface for adc-history"""
    temp_str1 = "history:0c2,5c6,10c5,11c6,2c1,7c8"
    print(temp_str1)
    temp_str = temp_str1.split(":")[1]
    print(temp_str)
    if len(temp_str) != 0:
        temp_str = temp_str.split(',')
        print(temp_str)
        #for pair in plotting_list:
        #    vata, ch = pair.split('c')
        #    vata = int(vata)
        #    ch = int(ch)

if __name__ == "__main__":
    string_split()