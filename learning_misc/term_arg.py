"""Understanding how to import arguments at execution.

    This uses sys.argv, there is also args.parser?
    Here
    The Arguments start at array 1 as 0 is for script
    This also works for dragging the file.

"""

import sys
import h5py

FNAME ="fname"
def print_fname(FNAME):
    print(FNAME)
    f = h5py.File(FNAME, 'r')
    k = f.keys()
    d = f.get('data')
    d_key = d.keys()
    print(d_key)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        FNAME = str(input('Enter the filename:'))
        FNAME=fname.rstrip() # removing white space in end when dragging file/folder
    elif len(sys.argv) == 2:
        FNAME = sys.argv[1]
    else:
        print("Usage Error: Too many arguments" )

    print_fname(FNAME)
     



'''
# This is using our fname arguments. 
import sys
import h5py


fname = str(sys.argv[1])
print(fname)


f = h5py.File(fname,'r')
k = f.keys()
d = f.get('data') 
d_key = d.keys()
print(d_key)
'''