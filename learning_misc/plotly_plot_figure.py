""" plotly, array slicing example

"""
import numpy as np
import plotly.express as px


#fig.add_trace(go.Scatter(x=list(self.fpga_rate_time), y=list(self.fpga_rates)))

def plotly_plot_figure():
    xlist = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    x = list(xlist)
    ylist = np.array([10, 20, 11, 19, 12, 18, 13, 17, 14, 16, 15, 15])
    y = list(ylist)
    fig = px.scatter(x=x,y=y)
    fig.show()

    b = fig.data[0]
    print(b)
    print(type(b))

    a = (fig.data[0],['x'][-5:-1])
    print(a)
    print(type(a))

    avg_rate = np.mean(fig.data[0]['x'][-5:-1])
    print(avg_rate)

def plotly_plot_figure_multi():
    """ Plotly figure to help understand and modify interface.

    Multiple plots in the plotly plot.

    Returns
    -------

    """
    xlist = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    x = list(xlist)
    ylist = np.array([[10, 20, 11, 19, 12, 18, 13, 17, 14, 16, 15, 15],[5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]])
    #ylist = np.array([[1,2,3],[4,5,6]])
    y = list(ylist)
    fig = px.scatter(x=x,y=y)
    return(fig)

def plotly_add_shape():
    #fig = plotly_plot_figure_multi()
    xlist = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    x = list(xlist)
    ylist = np.array([10, 20, 11, 19, 12, 18, 13, 17, 14, 16, 15, 15])
    #ylist = np.array([[1,2,3],[4,5,6]])
    y = list(ylist)
    fig = px.scatter(x=x,y=y)
    fig.add_vline(x = 2.5, line_width=3, line_dash="dash",line_color="green")
    fig.show()

if __name__ == "__main__":
    plotly_add_shape()