""" Few Array issues.

write the csv file.
close csv file.
append csv file.
rename csv file.
"""
import csv
import numpy as np
import plotly.express as px
import os

INDEX = np.array([1,2,3,4,5,6,7,8,9,10])
NAME  = ["Apple","Ball","Cat","Dog","Elephant","Fish","Gun","Horse","Indigo","Joker"]
SCORE = np.array([86,90,20,39,53,66,96,53,86,89])

B = []
def temp():
    a = []
    a.append((2,3))
    print(a[0])
    print(a)
    print(type(a))
    if a is None:
        print('None')
    else:
        print( 'Else')

def temp2(val,layer):
    temp_list = [0,0,0,0,0,0]
    #print(temp_list)
    #print(type(temp_list))
    temp_list[layer]=val

    B.append(temp_list)
    #print(B)
    #print(type(B))



def temp3():
    print('**** TEMP 3 ****')
    c = np.array(B)
    print(c)
    print(type(c))
    print(c.shape)
    for i in range(6):
        print(i)
        layer_column = c[:,i]
        print(layer_column)
        print(type(layer_column))
        newval = layer_column[layer_column !=0]
        print(newval)
        print('---')

def temp4():
    temp()
    val = 20
    layer = 2
    temp2(val, layer)

    val = 10
    layer = 3
    temp2(val, layer)

    val = 40
    layer = 1
    temp2(val, layer)
    val = 11
    layer = 0
    temp2(val, layer)
    val = 50
    layer = 2
    temp2(val, layer)

    val = 30
    layer = 2
    temp2(val, layer)

    val = 40
    layer = 2
    temp2(val, layer)
    temp3()

def temp5():
    x = [1,2,3,4,5,6,7,8,9,10]
    y1 = [12,34,5,12,44,23,56,77,88,55]
    y2 = [23,44,56,77,88,93, 44,32,88,55]
    y = [y1,y2]

    fig = px.scatter(x=x,y=y)
    fig.show()

def temp6():
    a =[]
    a.append('1')
    a.append('2')
    print(f"connected to {a}")

def temp7():
    a = [1,2,3,4,5]

    for b in a:
        print(b)

def temp8():
    """ check float"""
    a1 = 20
    a2 = 15
    b1 = 50
    b2 = 34
    frac = float(a1-a2) / float(b1-b2)
    print(frac)

def temp9():
    for i in range(5):
        print(i)

def temp10(val):
    """ Test if an item in list"""
    list1 = ["L0","L1","L2","L3","L4","L5"]
    random_list = [True,False,False,False,False,False]
    print(random_list)
    random_list[0]=(True if val in list1 else False)
    print(random_list)

if __name__ == "__main__":
    temp10('L7')