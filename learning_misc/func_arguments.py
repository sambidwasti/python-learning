"""
A quick example to understand the
    function argument unpacking.
    * is for list,tuple,etc.
    ** is for dictionary. This example focuses on Dictionary.

    This is very useful as the parameters and arguments for
    plot, style, etc are defined this way and
"""


def print_vector(x=8, y=9, z=10):
    """
    Print Vector function.

    Parameters
    ----------
    x : int, optional
        Description of 'x' value. (the default is 8)
    y : int
        y value
    z : int
        z value
    """
    print('%s, %s, %s' % (x, y, z))


dict_val = {'x': 0, 'z': 1, 'y': 2}  # Note: The order doesn't matter.
dict_val1 = {'y': 4, 'x': 3}  # Note: As we have keywords and not parameters, missing doesn't matter.
dict_val2 = {'x': 5, 'a': 6, 'y': 7}  # Note: This generates an error as we do not have a keyword 'a'.

if __name__ == "__main__":
    print('@@')
    print_vector(**dict_val)
    print('--')
    print_vector(**dict_val1)
    print('**')
    print_vector(**dict_val2)
