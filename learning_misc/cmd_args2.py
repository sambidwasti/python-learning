"""Additional arge parse testing.

    Testing program for the interface.
"""
import sys

# Current Number of layers.
NO_OF_CUR_LAYERS = 6

#All 10 Layer Server's Host Addresses
L0_HOST = "10.10.0.20"
L1_HOST = "10.10.0.21"
L2_HOST = "10.10.0.22"
L3_HOST = "10.10.0.23"
L4_HOST = "10.10.0.24"
L5_HOST = "10.10.0.25"
L6_HOST = "10.10.0.26"
L7_HOST = "10.10.0.27"
L8_HOST = "10.10.0.28"
L9_HOST = "10.10.0.29"


LAYER_HOST_ARRAY = [L0_HOST,L1_HOST,L2_HOST,L3_HOST,L4_HOST,L5_HOST,L6_HOST,L7_HOST,L8_HOST,L9_HOST]

#Array for Local Hosts.
LOCAL_HOST_ARRAY = ["localhost","localhost","localhost","localhost","localhost",
                    "localhost","localhost","localhost","localhost","localhost"]

# List of all 10 local ports. (9998 is the port address for the layer)
L0_PORT = "9998"
L1_PORT = "9980"
L2_PORT = "9981"
L3_PORT = "9982"
L4_PORT = "9983"
L5_PORT = "9984"
L6_PORT = "9985"
L7_PORT = "9986"
L8_PORT = "9987"
L9_PORT = "9988"


#Local Ports Array
LOCAL_PORTS = [L0_PORT,L1_PORT,L2_PORT,L3_PORT,L4_PORT,L5_PORT,L6_PORT,L7_PORT,L8_PORT,L9_PORT]
#Layer Ports Array.
LAYER_PORTS = [L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT]

# NOW we redefine the array for no. of layers.
LAYER_HOST_ARRAY = LAYER_HOST_ARRAY[:NO_OF_CUR_LAYERS]
LAYER_PORTS = LAYER_PORTS[:NO_OF_CUR_LAYERS]
LOCAL_HOST_ARRAY = LOCAL_HOST_ARRAY[:NO_OF_CUR_LAYERS]
LOCAL_PORTS = LOCAL_PORTS[:NO_OF_CUR_LAYERS]

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Run the data receiver")
    parser.add_argument("-L",type = str,default = '0,1,2,3,4,5',help="Default is all 6 (0-5), if not, enter layer numbers separated by ','.")
    parser.add_argument("--testbench", action='store_true',help=" Running for testing.")
    parser.add_argument("-N", default = NO_OF_CUR_LAYERS, help= "No of Layers to simulate for testbench. Default is 6. ")
    parser.add_argument("--host", type=str, default="localhost", help="Silicon layer host IP")
    parse.add_argument("--2host", type=str, defaul=2 )
    args = parser.parse_args()

    #print(' The command line arguments are : ')
    if not args.testbench:
        if not args.host:
            print("no host")
            # select the layers if L is supplied.
            # Layer is a coma separated string.
            temp_layer_string = args.L
            layer_nos = temp_layer_string.split(',')
            #check if valid layer nos.
            for x in layer_nos:
                if not 0<= int(x)<6:
                    print(x)
                    print('Invalid Layer Numbers. Layer Number should be between 0-5 separated by , for more than 1')
                    break
        else:
            print('Host', args.host)

        #logger.DEBUG("Creating CLIENT.")
        #iface = Interface(app, Client(hosts=loc_host_arr, data_ports=loc_port_arr),
         #                 interface_version=interface_version)
        # This needs to be updated.
        #response = iface.client.send_recv("connect")  # this is running at
        #print(f"Connected client to server; receieved response {response}")
       #logger.info(f"Connected client to server; receieved response {response}")
    else:
        logger.DEBUG("Creating layout-only testbench.")
        loc_host_arr = LOCAL_HOST_ARRAY[:args.N]
        loc_port_arr = LOCAL_PORTS[:args.N]
        logger.DEBUG("Creating CLIENT.")
        iface = Interface(app, Client(hosts=loc_host_arr, data_ports=loc_port_arr),
                          interface_version=interface_version)
        # This needs to be updated.
        response = iface.client.send_recv("connect")  # this is running at
        print(f"Connected client to server; receieved response {response}")
        logger.info(f"Connected client to server; receieved response {response}")

