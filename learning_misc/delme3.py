""" This is a test tab for a layouts"""
import dash_html_components as html

def build_delme3():
    return html.Div(
        id= "delme3",
        className="layer-panel",
        children=[
            html.H5(children="Layer"),
            html.Span(id="tl1-led", className="delme3 delme3_off", children=["1"]),
            html.Span(id="tl2-led", className="delme3 delme3_on",children=["2"]),
            html.Span(id="tl3-led", className="delme3", children=["3"])
            ]
    )