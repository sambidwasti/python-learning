"""
    Python script to see the example of a structured array.
"""

# This python scripts to see the example of structured arrays.
# This has while and for loop.

import numpy as np

if __name__ == "__main__":
    x = np.zeros(3, dtype={'names':['col1', 'col2'], 'formats':['i4','f4']})
    print(type(x))
    print(x)

    a = x.dtype.names
    print(type(a))
    print(a)

    print(a.shape)