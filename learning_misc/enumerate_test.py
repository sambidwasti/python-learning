""" Enumerate example

    Example for enumerate function.

"""

def enum_test1(days):
    """ Print enumerate days and type.

    Parameters
    ----------
    days : str list
        List of string array.
    """
    enum_days = enumerate(days)
    print(type(enum_days))
    print(enum_days)

def enum_test2(days):
    """ Understand the enumerate in a loop.

    Parameters
    ----------
    days : str list
        List of string array.
    """
    for a, exist in enumerate(days):
        print(a,exist)

def enum_test3(days):
    """ Change the starting value of enumerate

    Parameters
    ----------
    days : str list
        List of string array.

    """
    enum_days = enumerate(days, 5)
    print(list(enum_days))

def enum_test4():
    """ Test for 2 d array.
    """
    double_days = { ('Sun','Jan'),('Mon','Feb'),('Tues','Mar') }
    for a, exist in enumerate(double_days):
        print(a,exist)
        print(a, exist[0], exist[1])

def enum_test5():
    """ Test for 2 d array.
    """
    double_num = { (0,10),(1,11),(2,12) }
    for a, exist in enumerate(double_num):
        pair = exist
    print("--")
    print(pair)
    print ("++")
    return_str=""
    print(return_str)
    return_str += f"A{pair[0]:02d}-{pair[1]:02d}, "
    print(return_str)
    return_str = return_str[0:-2]
    print(return_str)

if __name__ == "__main__":
    days = {'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'}
    print(" Test 1: Enumerating days")
    enum_test1(days)
    print(" Test 2: Enumerating days in a forloop")
    enum_test2(days)
    print(" Test 3: Change the default counter to 5")
    enum_test3(days)
    print(" Test 4: 2D array of exist.")
    enum_test4()
    print(" Test 5: String manipulation on test4.")
    enum_test5()
