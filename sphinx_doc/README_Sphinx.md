# Python Learning Repo
This is a repository of Python Learning Series. 


## Main Components are  ##

### Learning Dash Devices
  This consists of scripts dedicated to learning dash, html, and dash devices. 

### Learning Misc
  Temporary scripts used to learn various aspects of python. 

# Python Notebooks
## Basics
Basic functions.
## Python Courses (python course 2021)
From the 2021 course

### Code
Of course abide by [PEP 8](https://www.python.org/dev/peps/pep-0008/), with the exception
of maximum line lengths. We are using 90 characters as our maximum line length. For bonus
points, it is recommended that code is formatted with
[python black](https://pypi.org/project/black/).

