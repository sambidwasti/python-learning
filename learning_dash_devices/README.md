<!-- Output copied to clipboard! -->



[TOC]






1.  Callbacks
*   Callbacks are imported via dash?.. 
*   Typically, they are called via decorators.
*   Usually, there is an output, input. 
    *   Which means that the callbacks are triggered when an input changes or output changes. 
    *   Usually, Output is first and Input second. 
    *   From Interface, we can decipher that 
        *   We can have more than 1 callbacks. 
        *   Or more than 1 input for an output.. (Need to verify this).
*   The toggles on graphs are usually from plotly-express. 
*   There are some plotting etiquettes to follow as well as when to import data to make it run smoother and faster. But these are for later.
2. Tutorial Notes: Layout

<h4>General</h4>




1. Everything (Almost everything is html component).
2. All functions/elements return html components. 
    1. You can do all the imports, fancy reads, customization but it has to return html component (or it goes in the html.div).

<h4>Layout (tutorial_layout_1.py, tutorial_layout_2.py)</h4>




1. The layout is composed of a tree of "components" like html.Div and dcc.Graph.
2. The dash_html_components library has a component for every HTML tag. The html.H1(children='Hello Dash') component generates a <h1>Hello Dash</h1> HTML element in your application.
3. Not all components are pure HTML. The dash_core_components describe higher-level components that are interactive and are generated with JavaScript, HTML, and CSS through the React.js library.
4. Each component is described entirely through keyword attributes. Dash is _declarative_: you will primarily describe your application through these attributes.
5. The children property is special. By convention, it's always the first attribute which means that you can omit it: html.H1(children='Hello Dash') is the same as html.H1('Hello Dash'). Also, it can contain a string, a number, a single component, or a list of components.
6. The fonts in your application will look a little bit different than what is displayed here. This application is using a custom CSS stylesheet to modify the default styles of the elements. You can learn more in the[ css tutorial](https://dash.plotly.com/external-resources), but for now you can initialize your app with

<h4>Styles (tutorial_layout_3py)</h4>




1. The style property in HTML is a semicolon-separated string. In Dash, you can just supply a dictionary.
2. The keys in the style dictionary are[ camelCased](https://en.wikipedia.org/wiki/Camel_case). So, instead of text-align, it's textAlign.
3. The HTML class attribute is className in Dash. ?
4. The children of the HTML tag is specified through the children keyword argument. By convention, this is always the** _first_ argument** and so it is often omitted.

<h4>Fig (tutorial_layout_4.py)</h4>




*   The dash_core_components library includes a component called Graph.
*   Graph renders interactive data visualizations using the open source[ plotly.js](https://github.com/plotly/plotly.js) JavaScript graphing library. Plotly.js supports over 35 chart types and renders charts in both vector-quality SVG and high-performance WebGL.
*   The figure argument in the dash_core_components.Graph component is the same figure argument that is used by plotly.py, Plotly's open source Python graphing library. Check out the[ plotly.py documentation and gallery](https://plotly.com/python) to learn more.

<h4>Markdown (tutorial_layout_5.py)</h4>




*   While Dash exposes HTML through the dash_html_components library, it can be tedious to write your copy in HTML. For writing blocks of text, you can use the Markdown component in the dash_core_components library. Create a file named app.py with the following code:

<h4>More visualization : Core Components (tutorial_layout_6.py)</h4>




*   The dash_core_components includes a set of higher-level components like dropdowns, graphs, markdown blocks, and more.
*   Like all Dash components, they are described entirely declaratively. Every option that is configurable is available as a keyword argument of the component.
*   We'll see many of these components throughout the tutorial. You can view all of the available components in the[ Dash Core Components Gallery](https://dash.plotly.com/dash-core-components)

<h4>Help</h4>




*   Dash components are declarative: every configurable aspect of these components is set during instantiation as a keyword argument. Call **help** in your Python console on any of the components to learn more about a component and its available arguments.

>>> help(dcc.Dropdown)

<h4>Summary</h4>


The layout of a Dash app describes what the app looks like. The layout is a hierarchical tree of components. The dash_html_components library provides classes for all of the HTML tags and the keyword arguments describe the HTML attributes like style, className, and id. The dash_core_components library generates higher-level components like controls and graphs.

For reference, see:



*   [dash_core_components gallery](https://dash.plotly.com/dash-core-components)
*   [dash_html_components gallery](https://dash.plotly.com/dash-html-components)



3. 
Tutorials-layout
Examples form tutorial series: Chapter2: Layout

<h4>tutorial_layout_1.py (graphs, plotly express)</h4>




*   This has basic objects (graph and text)

<h4>tutorial_layout_2.py (graphs, plotly express)</h4>




*   This is used to modify tutorial_layout_1 cosmetics. Colors, etc .
*   It uses Div and style.

<h4>tutorial_layout_3.py (table, pd)</h4>




*   This reads in table. 
*   This also outlines how a function (generate_table) is used.
    *   The function creates a table and returns the html table component.

<h4>tutorial_layout_4.py (fig, plotly express)</h4>




*   This reads in table. 

<h4>tutorial_layout_5.py (markdown)</h4>




*   This is an example of markdown text.

<h4>tutorial_layout_6.py (dcc, dropdown, multiple dropdown, label, column-count, slider, checklist)</h4>




*   More dash core components.
*   columncount at style shows how many columns to separate things.
*   





4. Tutorial Notes : Callback

<h4>General</h4>


This chapter describes how to make your Dash apps using **callback functions: Python functions** that are **automatically called by Dash** whenever an **input component's property changes**.

For optimum user-interaction and chart loading performance, production Dash applications should consider the[ Job Queue](https://plotly.com/dash/job-queue),[ HPC](https://plotly.com/dash/big-data-for-python),[ Datashader](https://plotly.com/dash/big-data-for-python), and[ horizontal scaling](https://plotly.com/dash/kubernetes) capabilities of Dash Enterprise

<h4>Concept (Current understanding)</h4>




*   The callbacks are decorators of app. 
    *   Need to know these more properly as i progress along. 
*   The parameters define the change in input component's property
    *   The function along the decorators are executed when any change happens to the parameters.
    *   There is a** component id **and **component property** that are read/updated. 
    *   The function does its thing and should be understood on a case by case basis.
*   Usually, a call back has input/output. 
    *   There can be multiple input or output.
        *   BUT ITS PREFERRED TO HAVE 1 OUTPUT at a time. 
            *   Can run parallel.
            *   Removes unnecessary confusion.
    *   The function that follows the decorator must have input parameters in order of defined input in the decorator.
    *   Usually, output is defined first.
*   tutorial_callback_1.py: A simple callback for updating an **output text **for an **input text.**





5. Tutorial Callback

<h4>tutorial_callback_1.py (basic callback, input(), output())</h4>




*   a simple text updated when input is updated.
*   app.callback
    *   Input()
    *   Output()

**Let's break down this example:**



1. The "inputs" and "outputs" of our application's interface are described declaratively as the arguments of the **@app.callback decorator**.
1. In Dash, the inputs and outputs of our application are simply the properties of a particular component. In this example, our input is the "**value**" property of the component that has the **ID **"**my-input**". Our output is the "**children" **property of the component with the **ID "my-output".**
2. Whenever an **input property changes,** the **function that the callback decorator wraps **will **get called automatically**. Dash provides the function with the new value of the input property as an input argument and Dash updates the property of the output component with whatever was returned by the function.
3. **The component_id and component_property keywords are optional** (there are only two arguments for each of those objects). They are included in this example for clarity but will be omitted in the rest of the documentation for the sake of brevity and readability.
4. **Don't confuse the dash.dependencies.Input object and the dash_core_components.Input object. **The former is just used in **these callbacks** and the latter is an actual component.?
5. Notice how we don't set a value for the children property of the my-output component in the layout**. **When the Dash app starts, **it automatically calls all of the callbacks with the initial values of the input components** in order to populate the initial state of the output components.** **In this example, if you specified something like html.Div(id='my-output', children='Hello world'), it would get overwritten when the app starts**.**
6. It's sort of like programming with Microsoft Excel: whenever an input cell changes, all of the cells that depend on that cell will get updated automatically. This is called "Reactive Programming"**.**
7. Remember how every component was described entirely through its set of keyword arguments? Those properties are important now.** **With Dash interactivity, we can dynamically update any of those properties through a callback function. Frequently we'll update the children of a component to display new text or the figure of a dcc.Graph component to display new data, but we could also update the style of a component or even the available options of a dcc.Dropdown component!

<h4>tutorial_callback_2.py (basic callback, input(), output())</h4>




6. 
a simple text updated when input is updated.


7. 
app.callback


    1. Input()
    2. Output()

**Let's break down this example:**



*   In this example, the "value" property of the Slider is the input of the app and the output of the app is the "figure" property of the Graph. **Whenever the value of the Slider changes, Dash calls the callback function update_figure with the new value.** The function filters the dataframe with this new value, constructs a figure object, and returns it to the Dash application.

<h5>**Few nice patterns (panda filters, memory management, layout-transition) **</h5>




1. We're using the[ Pandas](http://pandas.pydata.org/) library for importing and filtering datasets in memory.
2. We load our dataframe at the start of the app: df = pd.read_csv('...'). This dataframe **df **is in the **global state of the app **and can be read inside the callback functions.
3. Loading data into memory can be expensive. By loading querying data at the start of the app instead of inside the callback functions, we ensure that this operation is only done when the app server starts.** **When a user visits the app or interacts with the app, that data (the df) is already in memory. If possible, expensive initialization (like downloading or querying data) should be done in the global scope of the app instead of within the callback functions.
4. **The callback does not modify the original data, **it just creates copies of the dataframe by filtering through **pandas filters.** This is important: **_your callbacks should never mutate variables outside of their scope_. **If your callbacks modify global state, then one user's session might affect the next user's session and when the app is deployed on multiple processes or threads, those modifications will not be shared across sessions.
5. We are turning on transitions with layout.transition to give an idea of how the dataset evolves with time: transitions allow the chart to update from one state to the next smoothly, as if it were animated.

<h4>tutorial_callback_3.py (multiple input(), 1 output()) </h4>


<h5><span style="text-decoration:underline;">style:display, dropdown form array, dcc.graph </span></h5>


In Dash, any "Output" can have multiple "Input" components. Here's a simple example that binds five Inputs (the value property of 2 Dropdown components, 2 RadioItems components, and 1 Slider component) to 1 Output component (the figure property of the Graph component). Notice how the app.callback lists all five dash.dependencies.Input inside a list in the second argument



*   Style has the display component. 
*   It also has an example of using a dropdown to list components from an array.
*   The difference between Value (used in callback with Capital)

**In this example**



*   The update_graph function gets called whenever the value property of the Dropdown, Slider, or RadioItems components change.
*   The input arguments of the update_graph function are the new or current value of each of the Input properties, in **the order that they were specified**.
*   Even though only a single Input changes at a time (a user can only change the value of a single Dropdown in a given moment), Dash collects the current state of all of the specified Input properties and passes them into your function for you. Your callback functions are always guaranteed to be passed the representative state of the app.

<h4>tutorial_callback_4.py (multiple output(), 1 input))</h4>


<h5>html.Tr, html.Td, html.Th</h5>




*   Example for multiple output.
*   Updates the **dcc.input**'s value.
*   The table components might have to be properly wrapped. 
*   Th ( Tr ( Td)))
    *   html.Tr (Table Row)
    *   html.Td (Data cell)
    *   html.Th (Header)

**A word of caution: it's not always a good idea to combine Outputs, even if you can:**



*   If the **Outputs depend on some but not all of the same Inputs**, keeping them separate can avoid unnecessary updates.
*   **If they have the same Inputs but do independent computations with these inputs**, **keeping the callbacks separate can allow them to run in parallel.**

<h4>tutorial_callback_5.py (chained callbacks)</h4>


**<span style="text-decoration:underline;">RadioItems, output(values, list, etc), html.Hr</span>**



*   html.Hr is a horizontal Rule (Thematic Break). Here it looks like a horizontal line.
*   This has 2 radioitems. Depending on the option in 1 (America/Canada), the option in 2 changes. 
*   And depending on the options, there is a text in the end.
*   So the 1st radioitem is the Input. 
*   Then we have 2 outputs, the options of cities (2nd radioitems) and the text.
*   3 Callbacks.
    *   The first callback **updates the available options in the second RadioItems** component based off of the selected value in the input f**irst RadioItems component.**
    *   **The second callback sets an initial value** when the **options property changes:** it sets it to the first value in that options array.
    *   The final callback displays the text ( selected value of each component).
    *   **If you change the value of the countries RadioItems component**, **Dash will wait until the value of the cities component is updated before calling the final callback**. This prevents your callbacks from being called with inconsistent state like with "America '' and "Montréal". **Why**?
*   Do not understand the order of the callbacks run this way.
    *   1st CB (Input 1:value, Output 2: option)
    *   2nd CB (Input 2:option, Output2:value)
    *   3rd CB (Input 1:value, Input2:value, Output:Text)

<h4>tutorial_callback_6.py and tutorial_callback_7.py (state)</h4>


<h5>state, button</h5>




*   In some cases, you might have a "form"-type pattern in your application. In such a situation, you might want to read the value of the input component, but only when the user is finished entering all of his or her information in the form.
*   example 6 is where it updates everytime the input is changed to some degree. 
*   example 7 is where the update happens after the toggle of the button.

**In the example:**



*   Example 6 will just update the 1 output when any of the 2 input changes. Its pretty straight forward.
*   Example 7 has a state for the **2 inputs** with an addition of the button.
*   Changing text in the dcc.Input boxes won't fire the callback but clicking on the button will. 
    *   The current values of the dcc.Input values are still passed into the callback even though they don't trigger the callback function itself.
    *   Note that we're triggering the callback by listening to the n_clicks property of the html.Button component. 
    *   n_clicks is a property that gets incremented every time the component has been clicked on. 
    *   **It is available in every component in the dash_html_components library.**
*   





8. Examples
<h4>
    ex_1.py (Font style, Slider, style)</h4>


*   This is built on example 1. 
*   Editing font in html using style

<h4>ex_2.py (Div, children)</h4>




*   built on ex_1.py (which is built on example1.py)
*   Simple script where the div is filled and children are inside it. 
*   Div is a fictitious component that defines the space where each component/object is placed.
*   For dash, the children is defined as ([HERE](https://dash.plotly.com/layout)):

    _The children of the HTML tag is specified through the children keyword argument. By convention, this is always the first argument and so it is often omitted.	_

*   When we use the children, we can edit the div area.

**NOTE : The indentation matters. Mainly where the styles are placed.**

<h4>ex_3.py ( H1, H2, H3, H4, H5, H6, style, position, layout, margin, P, color)</h4>




*   Starting to learn components in the interface. This mostly deals with positioning. 
*   paragraph is capital P.
*   Positioning is extremely annoying
    *   We need to place everything in html.div in order to use style to have them where we want.
    *   The positioning is a little confusing but this example has some usage that might help. 
    *   Color can be used from [CSS color](https://www.w3schools.com/cssref/css_colors.asp).

ex_4.py ( Button, Markdown )



*   This is based on ex_3 and dash tutorial (tutorial_basic_callback_1)

tutorial_basic_callback.py (basic callback)



*   This is an example from the dash tutorial for basic callback.





9. Dash-devices html components

    This is going to populate the various [dash html components](https://dash.plotly.com/dash-html-components) used.


[html.Div](https://dash.plotly.com/dash-html-components/div): 





10. Dash Core Components

    This is going to populate the various dash core components. 


    dcc.slider : 
