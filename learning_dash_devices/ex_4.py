import dash
import dash_devices
from dash_devices.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc

app = dash_devices.Dash(__name__)
app.config.suppress_callback_exceptions = True


app.layout = html.Div([

    html.Div(children=[

        html.Div(children=[
            html.H1(children='Hello Universe'),
            html.H2(children='Hello Milky Way'),
            html.H3(children='Hello Helio'),
            html.H4(children='Hello Earth'),
            html.H5(children="Hello 'Murica"),
            html.H6(children='Hello World')
        ],
        style = {
            'float': 'left',
            'marginRight': '10px',
            'marginTop': '10px',
            'marginBottom':'10px',
            'position': 'absolute',
            'margin': 'top',
            'height':'200%'
        }
        ),

        html.Div(children=[
            html.Button('Submit',
                        id='btn-submit',
                        n_clicks=0,
                        style={
                            'color': 'Red',  # 1st letter is capital
                            'font-size': '200%',  # 100% is regular size.may be 12?
                            'float': 'right',
                            'marginRight': '10px',
                            'marginTop': '10px',
                            'position': 'relative'
                        }
                        ),

        ],
        style={
            'float': 'right',
            'marginRight': '10px',
            'marginTop': '10px',
            'position': 'relative',
            'margin': 'top'
        }
        )
    ]),

    html.Div(children=[
        html.P(' Dash: A web application framework for Python.')
    ],
    style = {
    'color': 'Blue',  # 1st letter is capital
    'font-size': '150%',  # 100% is regular size.may be 12?
    'background-color': 'Yellow',  # 1st letter is Capital
    'border': '1px solid Black',
    'width': '50%',  # looks like % of the max width (window not page) ; for fixed use pixel
    'height': '50%',  # seems useless here.
    'top': '150px',
    'left': '50px',
    'text-align': 'center',
    'position': 'relative',
    'margin':'0 auto',  # Centering the object (Div here)
    }
    ),

    html.Div([
        html.Button('Button 1', id='btn-1', n_clicks=0),
        html.Button('Button 2', id='btn-2', n_clicks=0),
        html.Button('Button 3', id='btn-3', n_clicks=0),
        html.Div(id='container-button-timestamp')
    ])
],
style = {
'background-color':'FloralWhite', # 1st letter is Capital
'border'       :'1px solid Black',
'width': '100%',  # looks like % of the max width (window not page) ; for fixed use pixel
'height': '200%',  # seems useless here.
'overflow':'hidden'
}
)



@app.callback_shared(Output('shared_slider_output', 'children'), [Input('shared_slider', 'value')])
def func(value):
    return value
'''
@app.callback(Output('container-button-timestamp', 'children'),
              Input('btn-nclicks-1', 'n_clicks'),
              Input('btn-nclicks-2', 'n_clicks'),
              Input('btn-nclicks-3', 'n_clicks'))

def displayClick(btn1, btn2, btn3):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'btn-nclicks-1' in changed_id:
        msg = 'Button 1 was most recently clicked'
    elif 'btn-nclicks-2' in changed_id:
        msg = 'Button 2 was most recently clicked'
    elif 'btn-nclicks-3' in changed_id:
        msg = 'Button 3 was most recently clicked'
    else:
        msg = 'None of the buttons have been clicked yet'
    return html.Div(msg)
'''
'''
@app.callback(Output('container-button-timestamp', 'children'),
              Input('btn-submit', 'n_clicks'))
def displayClick(submit):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'btn-submit' in changed_id:
        msg = 'Button 1 was most recently clicked'
    else:
        msg = 'None of the buttons have been clicked yet'
    return html.Div(msg)
'''

if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=5000)