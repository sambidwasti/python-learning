import dash_devices
from dash_devices.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc

app = dash_devices.Dash(__name__)
app.config.suppress_callback_exceptions = True

app.layout = html.Div([

    html.Div(children=[

        html.Div(children=[
            html.H1(children='Hello Universe'),
            html.H2(children='Hello Milky Way'),
            html.H3(children='Hello Helio'),
            html.H4(children='Hello Earth'),
            html.H5(children="Hello 'Murica"),
            html.H6(children='Hello World')
        ],
        style = {
            'float': 'left',
            'marginRight': '10px',
            'marginTop': '10px',
            'marginBottom':'10px',
            'positoin': 'absolute',
            'margin': 'top',
            'height':'200%'
        }
        ),

        html.Div(children=[
            html.Button('Submit',
                        id='submit-val',
                        n_clicks=0,
                        style={
                            'color': 'Red',  # 1st letter is capital
                            'font-size': '200%',  # 100% is regular size.may be 12?
                            'float': 'right',
                            'marginRight': '10px',
                            'marginTop': '10px',
                            'position': 'relative'
                        }
                        ),

        ],
        style={
            'float': 'right',
            'marginRight': '10px',
            'marginTop': '10px',
            'position': 'relative',
            'margin': 'top'
        }
        )
    ]),

    html.Div(children=[
        html.P(' Dash: A web application framework for Python.')
    ],
    style = {
    'color': 'Blue',  # 1st letter is capital
    'font-size': '150%',  # 100% is regular size.may be 12?
    'background-color': 'Yellow',  # 1st letter is Capital
    'border': '1px solid Black',
    'width': '50%',  # looks like % of the max width (window not page) ; for fixed use pixel
    'height': '50%',  # seems useless here.
    'top': '150px',
    'left': '50px',
    'text-align': 'center',
    'position': 'relative',
    'margin':'0 auto',  # Centering the object (Div here)
    }
    )
],
style = {
'background-color':'FloralWhite', # 1st letter is Capital
'border'       :'1px solid Black',
'width': '100%',  # looks like % of the max width (window not page) ; for fixed use pixel
'height': '200%',  # seems useless here.
'overflow':'hidden'
}
)


'''
    html.Div("Regular slider"),
    dcc.Slider(id='regular_slider', value=5, min=0, max=10, step=1, updatemode='drag'),
    html.Div(id='regular_slider_output'),

    html.Div("Shared input"),
    dcc.Input(id="shared_input", type="text", value=''),
    html.Div(id='shared_input_output'),

    html.Div("Regular input"),
    dcc.Input(id="regular_input", type="text", value=''),
    html.Div(id='regular_input_output'),
'''


@app.callback_shared(Output('shared_slider_output', 'children'), [Input('shared_slider', 'value')])
def func(value):
    return value

'''
@app.callback(Output('regular_slider_output', 'children'), [Input('regular_slider', 'value')])
def func(value):
    return value

@app.callback_shared(Output('shared_input_output', 'children'), [Input('shared_input', 'value')])
def func(value):
    return value

@app.callback(Output('regular_input_output', 'children'), [Input('regular_input', 'value')])
def func(value):
    return value
'''
if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=5000)

