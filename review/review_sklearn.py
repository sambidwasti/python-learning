from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
import pandas as pd

# Sample Data
data = {'Value': [50, 60, 80, 100, 200]}
df = pd.DataFrame(data)
print(df.info())
print(df)
# Initialize MinMaxScaler
scaler = MinMaxScaler()

# Apply Min-Max Scaling
df['MinMax_Scaled'] = scaler.fit_transform(df[['Value']])

print('MINMAX')
print(df)

# Initialize StandardScaler
scaler = StandardScaler()

# Apply Standardization
df['Standardized'] = scaler.fit_transform(df[['Value']])

print('STD')
print(df)