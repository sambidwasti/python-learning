import numpy as np
import os
import pandas as pd

'''
Challenge 1: Data Wrangling and Transformation

Task:
Merge two datasets (transactions.csv and customers.csv) to filter and analyze transaction amounts and calculate discounts.

Let me know when you’re done, and I’ll share the next challenge!

'''


def main():
    trans_csv = pd.read_csv("transactions.csv")
    cust_csv = pd.read_csv("customers.csv")

    merged_pd = pd.merge(trans_csv,cust_csv)
    print(trans_csv)
    print(cust_csv)

    print(merged_pd)

    merged_pd.to_csv("chal1_merged.csv", index=False)


if __name__ == '__main__':
    main()