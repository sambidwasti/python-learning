import pandas as pd
from sklearn.preprocessing import MinMaxScaler

# Example dataset
data = {
    'Name': ['Alice', 'Bob', 'Charlie', 'David', 'Eve', 'Alice'],
    'Age': [25, None, 30, 22, 35, 25],
    'Income($)': [50000, 60000, None, 45000, 80000, 50000]
}
df = pd.DataFrame(data)

# Step 1: Handle missing values (fill missing Age with mean, drop rows with missing Income)
df['Age'] = df['Age'].fillna(df['Age'].mean())  # Avoid inplace=True
df = df.dropna(subset=['Income($)'])  # Avoid chaining to be explicit

# Step 2: Remove duplicates
df = df.drop_duplicates()

# Step 3: Rename columns for consistency
df.rename(columns={'Income($)': 'Income'}, inplace=True)

# Step 4: Normalize the Income column
scaler = MinMaxScaler()
df['Normalized_Income'] = scaler.fit_transform(df[['Income']])

# Final cleaned and transformed dataset
print(df)