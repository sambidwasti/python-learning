'''Challenge 2: Data Aggregation

Task:
Use the sales.csv dataset to group and analyze product categories, 
summarizing revenue and identifying top-performing categories.


Step, read, create columns for revenue, and group.
'''

import numpy as np
import pandas as pd
import os


def main():

    # Read csv to pd
    sales_csv = pd.read_csv("sales.csv")
    

    #product_id  category  price  sold_units
    sales_csv["rev"] = sales_csv["price"]*sales_csv["sold_units"]
    print(sales_csv)

    grouped = sales_csv.groupby(by="category")["rev"].sum()
    print(grouped)

    # From the revenue,Electronics performed the most.

    # trans_csv = pd.read_csv("transactions.csv")
    # cust_csv = pd.read_csv("customers.csv")

    # merged_pd = pd.merge(trans_csv,cust_csv)
    # print(trans_csv)
    # print(cust_csv)

    # print(merged_pd)

    # merged_pd.to_csv("chal1_merged.csv", index=False)


if __name__ == '__main__':
    main()